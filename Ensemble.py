import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import VotingClassifier
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
from sklearn import metrics


# Read Data from file
train_file_path = "data/train.csv"
test_file_path = "data/test.csv"
train = np.genfromtxt(train_file_path,skip_header=True,delimiter=",")
test = np.genfromtxt(test_file_path,skip_header=True,delimiter=",")
train_x = train[:,0:4]
train_y = train[:,4]
test_x = test[:,0:4]
test_y = test[:,4]
kf= KFold(shuffle=True,n_splits=10)

def print_metrics(clf):
    y_pred = clf.predict(test_x)
    accuracy = metrics.accuracy_score(test_y, y_pred)
    print('Test Accuracy: ', accuracy * 100)
    # print confusion matrix
    CM = metrics.confusion_matrix(test_y, y_pred)
    print("Confusion Matrix:", end="\n")
    print(CM, end=' \n\n')
    return accuracy

def fit_model(clf):
    clf = clf.fit(train_x, train_y)
    cross_val_score(clf, train_x, train_y, cv=kf)
    return clf

#Task 1 -(a) Random Forest Classifier
def rf_model(n_estimators=10,max_depth =2):
    rf_clf = RandomForestClassifier(n_estimators=n_estimators,max_depth=max_depth)
    return fit_model(rf_clf)


#Task 1-(b) Ada Boost Classifier
def ada_boost_model(n_estimators =50,learning_rate=1):
    ada_boost_clf = AdaBoostClassifier(n_estimators=n_estimators,learning_rate=learning_rate)
    return fit_model(ada_boost_clf)

#Task 2 Neural Network Model
def nn_model(hidden_layer_sizes=(100,),activation='relu',max_iter=200):
    nn_clf = MLPClassifier(hidden_layer_sizes=hidden_layer_sizes,activation=activation,max_iter=max_iter)
    return fit_model(nn_clf)

#K Nearest Neighbours Model
def knn_model(n_neighbors=5):
    knn_clf = KNeighborsClassifier(n_neighbors=n_neighbors)
    return fit_model(knn_clf)

#Logistic Model
def logistic_model():
    return fit_model(LogisticRegression())

#Naive Bayes Model
def nb_model():
    return fit_model(GaussianNB())

#Decision Tree Model
def dt_model(max_depth=3):
    return fit_model(DecisionTreeClassifier(max_depth=max_depth))

def voting_classifier(estimators,weights=None,voting="hard"):
    return fit_model(VotingClassifier(estimators,voting=voting,weights=weights))


if __name__ == '__main__':
    #Task 1-

    #Task2 - Weighted and Unweighted Majority Vote
    estimators = []
    nn_estimator = nn_model()
    knn_estimator = knn_model()
    lr_estimator = logistic_model()
    nb_estimator= nb_model()
    dt_estimator= dt_model()
    estimators.append(('nn', nn_estimator))
    estimators.append(('knn',knn_estimator))
    estimators.append(('lr',lr_estimator))
    estimators.append(('nb',nb_estimator))
    estimators.append(('dt',dt_estimator))

    acc =[]
    for estimator in estimators:
        acc.append(print_metrics(estimator[1]))
    print(acc)
    print_metrics(voting_classifier(estimators=estimators,weights=[x /sum(acc) for x in acc]))
